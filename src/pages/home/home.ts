import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

class Produto {
  public id;
  public description;
  public bar_code;
  public image;
  public value;
}

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {  
  public produtos  = []
  public afterScan = false;
  public currentImage;
  public showImg = false;
  @BlockUI() blockUI: NgBlockUI;

  constructor(public navCtrl: NavController, private barcodeScanner: BarcodeScanner, private http : HttpClient) {                   
  }

  getCode(){
    this.barcodeScanner.scan().then((barcodeData) => {                  
      this.blockUI.start();
      let headers = new HttpHeaders({       
        'Content-Type':'application/json'
      });    
        this.http.request('get','http://gridsoft.com.br/rest/api/produtos/get',{body:{"bar_code":barcodeData}, headers:headers}).subscribe(res =>{          
          let produto = new Produto;          
          produto.id = res['id'];
          produto.description = res['description'];
          produto.bar_code = res['bar_code'];
          produto.image = res['image'];
          produto.value = res['value'];
          
          this.blockUI.stop();                              
          this.produtos.push(produto);          
        }, err => {
          this.blockUI.stop();
        })      
    }, (err) => {
      console.log(err);
    });
  }

  showImage(img){
    this.currentImage = img;
    this.showImg = true;
  }

  closeImg(){
    this.showImg = false;
  }
  
}
